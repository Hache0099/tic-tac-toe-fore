extends Node2D

const clickable = preload("res://Scenes/clickable_cell.tscn")

export(float) var cell_offset

var canBeDragged : bool = true
var canMove : bool = false
var canChange : bool = false
var lastMousePos : Vector2
var line_lenght : Array = [0.0,0.0]

onready var lines = $center_pos/lines

func _ready():
	pass

func _process(delta):
	var h_nodes = get_tree().get_nodes_in_group("horizontal")
	var v_nodes = get_tree().get_nodes_in_group("vertical")
	
	var length_v = v_nodes[0].get_line_lenght()
	var length_h = h_nodes[0].get_line_lenght()
	
	line_lenght[0] = length_h
	line_lenght[1] = length_v
	
	if canMove:
		var mousePos = get_global_mouse_position()
		position += mousePos - lastMousePos
		lastMousePos = get_global_mouse_position()
	set_clickable_pos()
#	if canChange:
#		changeSize(true)

func readyToPlay():
	canBeDragged = false
	
	var cell_size : Vector2 = Vector2(line_lenght[0] / 3.0, line_lenght[1] / 3.0)
	prints(line_lenght, cell_size)
	for y in 3:
		for x in 3:
			var b = clickable.instance()
			var pos : Vector2 = cell_size * Vector2(x,y) + cell_size / 2.0
			b.position = pos
			b.setSize(cell_size / 2.0 - Vector2.ONE * cell_offset)
			$center_pos/clickable_start/clickable_holder.add_child(b)

func set_clickable_pos():
	var h_nodes = get_tree().get_nodes_in_group("horizontal")
	var v_nodes = get_tree().get_nodes_in_group("vertical")
	
	var pos_x = h_nodes[0].get_point_position(0).x
	var pos_y = v_nodes[0].get_point_position(0).y
	
	$center_pos/clickable_start.set_position(Vector2(pos_x,pos_y))

func changeSize(grow_amount : float, is_vertical : bool):
	var h_nodes = get_tree().get_nodes_in_group("horizontal")
	var v_nodes = get_tree().get_nodes_in_group("vertical")

#	var mousePos = get_global_mouse_position()
#	var offset = mousePos - lastMousePos
#	lastMousePos = mousePos

	if is_vertical:
		get_tree().call_group("vertical","grow",grow_amount)
		for i in h_nodes.size():
			var length = v_nodes[0].get_line_lenght()

			h_nodes[i].position.y = length / 6.0 * pow(-1,i)
	else:
		get_tree().call_group("horizontal","grow",grow_amount)
		for i in v_nodes.size():
			var length = h_nodes[0].get_line_lenght()

			v_nodes[i].position.x = length / 6.0 * pow(-1,i)
	
	var length_v = v_nodes[0].get_line_lenght()
	var length_h = h_nodes[0].get_line_lenght()
	
	$center_pos/drag_area/CollisionShape2D.shape.extents = Vector2(length_h / 2.0, length_v / 2.0)

func _on_drag_area_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and canBeDragged:
		if event.is_pressed() and event.button_index == BUTTON_LEFT:
			canMove = true
			lastMousePos = get_global_mouse_position()
		else:
			canMove = false

func customRotation(angle : float):
	$center_pos.rotation_degrees = angle

func _on_TextureButton_pressed():
	canChange = true
	lastMousePos = get_global_mouse_position()
