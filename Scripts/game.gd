extends Control


func _ready():
	pass

func change_size(value : float, is_vertical : bool):
	$tioctactoe_board2.changeSize(value, is_vertical)

func _on_rotation_value_changed(value):
	$tioctactoe_board2.customRotation(value)

func _on_start_pressed():
	$tioctactoe_board2.readyToPlay()
	$Panel.hide()
