extends Area2D

var is_playing : bool = true
var turno : int = 0

func _ready():
	pass

func set_turno(_turno : int):
	turno = _turno

func setSize(size : Vector2):
	$CollisionShape2D.shape.extents = size
	var sprite_size = $Sprite.get_rect().size
	var scalar : float = min(size.x * 2, size.y * 2)
	var new_size : Vector2 = Vector2(scalar / sprite_size.x, scalar / sprite_size.y)
	$Sprite.set_scale(new_size)

func _on_clickable_cell_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed() and event.button_index == BUTTON_LEFT:
			$Sprite.frame = turno
			$Sprite.show()

			var new_turno = wrapi(turno + 1, 0, 2)
			get_tree().call_group("cells","set_turno", new_turno)
			$CollisionShape2D.disabled = true

