extends Control

var lastMousePos : Vector2
var canMove : bool = false
var canSize : bool = false

onready var board = $TextureRect

func _process(delta):
	if canMove:
		var mouse_pos = get_global_mouse_position()
		set_global_position(rect_position + mouse_pos - lastMousePos)
		lastMousePos = mouse_pos

func _on_TextureRect_gui_input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and event.button_index == BUTTON_LEFT:
			canMove = true
			lastMousePos = get_global_mouse_position()
		else:
			canMove = false
	
#	set_process(canMove)

func modi_margin(_margin : int):
	var dir = 1
	dir = -1 if _margin <= 1 else dir 
	board.set_margin(_margin, board.get_margin(_margin) + 10.0 * dir)
