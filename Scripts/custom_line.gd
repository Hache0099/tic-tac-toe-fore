extends Line2D

enum LINE_TYPE {HOR, VER}
export(LINE_TYPE) var dir

export(float) var min_lenght = 100.0
export(float) var max_lenght = 350.0

func _ready():
	var conv_dir = min_lenght / 2.0
	for i in get_point_count():
		var signDir = -(conv_dir * pow(-1,i))
		if dir == LINE_TYPE.HOR:
			set_point_position(i,Vector2(signDir,0))
		else:
			set_point_position(i,Vector2(0,signDir))

#func _process(delta):
#	$Label.set_text(str(points))

func get_line_lenght():
	return get_point_position(0).distance_to(get_point_position(1))

func grow(amount : float):
	var lenght = get_line_lenght()

#	if amount >= min_lenght and amount <= max_lenght:
	for i in get_point_count():
		var pos = get_point_position(i)
		var signAmount = -(amount * pow(-1,i))

		if dir == LINE_TYPE.HOR:
			set_point_position(i,Vector2(signAmount /2.0,pos.y))
		else:
			set_point_position(i,Vector2(pos.x,signAmount/2.0))
